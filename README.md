# RSHelpers

[![CI Status](https://img.shields.io/travis/Ruslan/RSHelpers.svg?style=flat)](https://travis-ci.org/Ruslan/RSHelpers)
[![Version](https://img.shields.io/cocoapods/v/RSHelpers.svg?style=flat)](https://cocoapods.org/pods/RSHelpers)
[![License](https://img.shields.io/cocoapods/l/RSHelpers.svg?style=flat)](https://cocoapods.org/pods/RSHelpers)
[![Platform](https://img.shields.io/cocoapods/p/RSHelpers.svg?style=flat)](https://cocoapods.org/pods/RSHelpers)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

RSHelpers is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'RSHelpers'
```

## Author

Ruslan, lmr.ruslan.soltanov@gmail.com

## License

RSHelpers is available under the MIT license. See the LICENSE file for more info.
